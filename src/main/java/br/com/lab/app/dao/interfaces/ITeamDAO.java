package br.com.lab.app.dao.interfaces;

import br.com.lab.app.dao.base.IBaseDAO;
import br.com.lab.app.model.Team;

public interface ITeamDAO extends IBaseDAO <Team> {

}
