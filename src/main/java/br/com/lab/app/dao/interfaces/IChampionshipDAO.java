package br.com.lab.app.dao.interfaces;

import br.com.lab.app.dao.base.IBaseDAO;
import br.com.lab.app.model.Championship;

public interface IChampionshipDAO extends IBaseDAO <Championship> {

}
