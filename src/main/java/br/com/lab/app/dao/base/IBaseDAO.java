package br.com.lab.app.dao.base;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

public interface IBaseDAO <T> {
	
	// CREATE
	@Transactional
	public boolean save(T entity);
	@Transactional
	public boolean saveList (List<T> entitys);
	
	// READ
	@Transactional
	public List<T> findAll();
	@Transactional
	public T findById (int id);
	public T findByName(String name);
	
	// UPDATE
	@Transactional
	public boolean update(T entity);
	@Transactional
	public void updateList (List<T> updates);
	
	// DELETE
	@Transactional
	public boolean delete(T entity);
	@Transactional
	public boolean deleteById(int id);
	@Transactional
	public boolean deleteByName(String name);
}
