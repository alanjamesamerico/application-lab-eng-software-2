package br.com.lab.app.dao.interfaces;

import br.com.lab.app.dao.base.IBaseDAO;
import br.com.lab.app.model.Coach;

public interface ICoachDAO extends IBaseDAO <Coach> {

}
