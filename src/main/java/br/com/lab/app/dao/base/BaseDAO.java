package br.com.lab.app.dao.base;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public abstract class BaseDAO <T> extends HibernateDaoSupport implements IBaseDAO <T> {
	
	private Class<T> entityClass;
	
	@SuppressWarnings("unchecked")
	public BaseDAO(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
		entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public boolean save(T entity) {
		try {
			getHibernateTemplate().save(entity);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public boolean saveList(List<T> entitys) {
		try {
			getHibernateTemplate().saveOrUpdateAll(entitys);
		} catch (Exception e) {
			System.err.println(e);
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public T findById(int id) {
		String query = "from " + entityClass.getSimpleName() + " where id= " + Integer.toString(id);
		List<T> list = getHibernateTemplate().find(query);
		return list.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public T findByName(String name) {
		
		String query = "from " + entityClass.getSimpleName() + " where LOWER(name)= LOWER("+"'"+name+"')";
		List<T> list = null;
		try {
			list = getHibernateTemplate().find(query);
			return list.get(0);
		} catch (Exception e) {
			System.err.println(e);
		}
		return list.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		String query = "from " + entityClass.getSimpleName();
		List<T> list = getHibernateTemplate().find(query);
		return list;
	}

	public boolean update(T entity) {
		try {
			getHibernateTemplate().update(entity);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public boolean delete(T entity) {
		try {
			getHibernateTemplate().delete(entity);
		} catch (Exception e) {
			System.err.println(e);
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public boolean deleteById(int id) {
		
		List<T> list = null;
		String query = "from " + entityClass.getSimpleName() + " where id= " + Integer.toString(id);
		
		try {
			list = getHibernateTemplate().find(query);
		} catch (Exception e) {
			System.err.println(e);
			return false;
		} try {
			getHibernateTemplate().delete(list.get(0));
		} catch (Exception e) {
			System.err.println(e);
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public boolean deleteByName(String name) {
		
		List<T> list = null;
		String query = "from " + entityClass.getSimpleName() + " where LOWER(name)= LOWER("+"'"+name+"')" ;
		
		try {
			list = getHibernateTemplate().find(query);
			getHibernateTemplate().delete(list.get(0));
		} catch (Exception e) {
			System.err.println(e);
			return false;
		}
		return true;
	}
	
	public void updateList(List<T> updates) {
		for (T entity : updates) {
			getHibernateTemplate().update(entity);
		}
	}
}
