package br.com.lab.app.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.lab.app.dao.base.BaseDAO;
import br.com.lab.app.dao.interfaces.IChampionshipDAO;
import br.com.lab.app.model.Championship;

@Repository
public class ChampionshipDAO extends BaseDAO <Championship> implements IChampionshipDAO {
	
	@Autowired
	public ChampionshipDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
}
