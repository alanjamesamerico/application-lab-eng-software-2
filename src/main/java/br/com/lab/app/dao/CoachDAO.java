package br.com.lab.app.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.lab.app.dao.base.BaseDAO;
import br.com.lab.app.dao.interfaces.ICoachDAO;
import br.com.lab.app.model.Coach;

@Repository
public class CoachDAO extends BaseDAO <Coach> implements ICoachDAO {
	
	@Autowired
	public CoachDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
}
