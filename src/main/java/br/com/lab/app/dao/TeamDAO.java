package br.com.lab.app.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.lab.app.dao.base.BaseDAO;
import br.com.lab.app.dao.interfaces.ITeamDAO;
import br.com.lab.app.model.Team;

@Repository
public class TeamDAO extends BaseDAO <Team> implements ITeamDAO {
	
	@Autowired
	public TeamDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
}
