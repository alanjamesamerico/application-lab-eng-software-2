package br.com.lab.app.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.lab.app.dao.base.BaseDAO;
import br.com.lab.app.dao.interfaces.IPlayerDAO;
import br.com.lab.app.model.Player;

@Repository
public class PlayerDAO extends BaseDAO <Player> implements IPlayerDAO {
	
	@Autowired
	public PlayerDAO(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
}
