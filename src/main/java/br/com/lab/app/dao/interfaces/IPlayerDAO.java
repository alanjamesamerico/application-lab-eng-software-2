package br.com.lab.app.dao.interfaces;

import br.com.lab.app.dao.base.IBaseDAO;
import br.com.lab.app.model.Player;

public interface IPlayerDAO extends IBaseDAO <Player>{

}
