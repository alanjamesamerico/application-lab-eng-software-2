package br.com.lab.app.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.lab.app.model.base.AbstractEntity;

@Entity
@Table
public class Team extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column (name = "path_flag_img")
	private String flagImg;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_coach")
	private Coach coach;
	
	@OneToMany (cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="fk_team")
	private List<Player> players;
	
	@ManyToMany
	@JoinTable (name = "team_championship", joinColumns = @JoinColumn(name = "team_id"), inverseJoinColumns = @JoinColumn(name = "champ_id"))
	private List<Championship> championships;
	
	public Coach getCoach() {
		return coach;
	}

	public void setCoach(Coach coach) {
		this.coach = coach;
	}

	public String getFlagImg() {
		return flagImg;
	}

	public void setFlagImg(String flagImg) {
		this.flagImg = flagImg;
	}
}
