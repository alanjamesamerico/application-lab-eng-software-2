package br.com.lab.app.model.base;

public interface IEntity {
	
	public long getId ();
	public void setId (Long id);
}
