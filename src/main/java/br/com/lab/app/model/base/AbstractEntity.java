package br.com.lab.app.model.base;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

@MappedSuperclass
public abstract class AbstractEntity implements IEntity {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column (nullable = false) // Not Null (NãoNulo)
	private String name;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public static class DateSerializerCustom extends JsonSerializer <Date> {
		
	    public void serialize(Date date, JsonGenerator jGenerator, SerializerProvider arg2) throws 
	        IOException, JsonProcessingException {

	        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy"); // HH:mm:ss
	        String formatDate = dateFormat.format(date);

	        jGenerator.writeString(formatDate);
	    }
	}
	
	public static class DateDeserializerCustom extends JsonDeserializer <Date> {
		
	    public Date deserialize(JsonParser jParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {

	        SimpleDateFormat format = new SimpleDateFormat("dd-mm-yyyy");
	        String date = jParser.getText();
	        
	        try {
	            return format.parse(date);
	        } catch (ParseException e) {
	            throw new RuntimeException(e);
	        }
	    }
	}
}
