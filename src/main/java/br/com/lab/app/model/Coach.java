package br.com.lab.app.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.lab.app.model.base.AbstractEntity;

@Entity
@Table
public class Coach extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column (nullable = false)
	int age;

	@JoinColumn(name = "fk_coach")
	@OneToOne (cascade = CascadeType.ALL, optional = true, fetch = FetchType.LAZY, mappedBy = "coach")
	private Team team;
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
