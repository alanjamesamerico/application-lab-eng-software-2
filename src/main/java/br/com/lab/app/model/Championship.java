package br.com.lab.app.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.lab.app.model.base.AbstractEntity;

@Entity
@Table
public class Championship extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column (nullable = false)
	private int rounds;
	
	@ManyToMany (mappedBy = "championships")
	private List<Team> teams;
	
	@Column (name = "date_start", nullable = false)
	@Temporal (TemporalType.DATE)
	private Date start;
	
	@Column (name = "date_end", nullable = false)
	@Temporal (TemporalType.DATE)
	private Date end;
	
	
	public int getRounds() {
		return rounds;
	}

	public void setRounds(int rounds) {
		this.rounds = rounds;
	}
	
	@JsonDeserialize(using = DateDeserializerCustom.class)
	public Date getStart() {
		return start;
	}
	
	@JsonSerialize(using = DateSerializerCustom.class)
	public void setStart(Date start) {
		this.start = start;
	}
	
	@JsonDeserialize(using = DateDeserializerCustom.class)
	public Date getEnd() {
		return end;
	}
	
	@JsonSerialize(using = DateSerializerCustom.class)
	public void setEnd(Date end) {
		this.end = end;
	}
}
