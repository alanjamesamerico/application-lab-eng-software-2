package br.com.lab.app.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.lab.app.model.base.AbstractEntity;

@Entity
@Table
public class Player extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column (nullable = false)
	private Integer age;
	private Float weight = 0f; // peso
	private Float height = 0f; // altura
	
	@ManyToOne (cascade = CascadeType.ALL)
	private Team team;
	
	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public Float getHeight() {
		return height;
	}

	public void setHeight(Float height) {
		this.height = height;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
}
