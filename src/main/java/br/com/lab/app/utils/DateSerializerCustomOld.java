package br.com.lab.app.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class DateSerializerCustomOld extends JsonSerializer <Date> {
	
    public void serialize(Date date, JsonGenerator jGenerator, SerializerProvider arg2) throws 
        IOException, JsonProcessingException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy"); // HH:mm:ss
        String formatDate = dateFormat.format(date);

        jGenerator.writeString(formatDate);
    }
}
