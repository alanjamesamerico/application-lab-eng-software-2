package br.com.lab.app.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Log {
	
	@SuppressWarnings("rawtypes") 
	public static Logger clazz (Class clazz) {
		return LoggerFactory.getLogger(clazz);
	}
}
