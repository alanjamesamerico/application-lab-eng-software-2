package br.com.lab.app.services.interfaces;

import java.util.List;

import br.com.lab.app.model.Championship;

public interface IChampionshipService {
    
    public List<Championship> findTeamByName(String nome);
    public List<Championship> findTeamByCoach(String nome);
}
