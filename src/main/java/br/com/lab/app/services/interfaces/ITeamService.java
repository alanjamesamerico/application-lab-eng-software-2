package br.com.lab.app.services.interfaces;

import java.util.List;

import br.com.lab.app.model.Team;

public interface ITeamService {
	
    public List<Team> findTeamByName(String nome);
    public List<Team> findTeamByCoach(String nome);
}
