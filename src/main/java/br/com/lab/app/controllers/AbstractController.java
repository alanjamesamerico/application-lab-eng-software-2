package br.com.lab.app.controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;

import org.springframework.stereotype.Controller;

@Controller
public abstract class AbstractController <T, I> {
	/*
	private 	Object serviceInterface;
    protected 	Class<T> entityClass;

	
	@SuppressWarnings("unchecked")
	protected AbstractController (I serviceInterface) {
		this.serviceInterface = serviceInterface;
        this.entityClass = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
	} 
	
	@SuppressWarnings("rawtypes")
	@ResponseStatus(value = HttpStatus.CREATED)
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add (@RequestBody T t) { 
		
		try {
			
			Class cls = Class.forName(entityClass.getName());
			Object o = cls.newInstance();
			
			Method method = this.serviceInterface.getClass().getDeclaredMethod("savePerson", entityClass);
			
			Field[] tFields = t.getClass().getDeclaredFields();
			
			System.out.println("\n\nFields do 't':\n");
			for (Field f : tFields) {
				System.out.println(f.getName());
			}
			
			if (t != null) {
				method.invoke(serviceInterface, o);
			}
			
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return "Object Creat Ok..";
	}
	
	@RequestMapping("/delete")
	public String remove (T t) {
		
		return null;
	}
	
	@ResponseBody
	@RequestMapping("/update")
	public String update(T t) {
		
		return null;
	}
	
	@RequestMapping(value = "/findById", method = RequestMethod.GET)
	@ResponseBody
	public String findOne(T t) {
		
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
	public String findAll() {
		
		return null;
	}
	*/
}
