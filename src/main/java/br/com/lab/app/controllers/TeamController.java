package br.com.lab.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.lab.app.controllers.interfaces.ITeamController;
import br.com.lab.app.services.interfaces.ITeamService;

public class TeamController implements ITeamController {
	
	@Autowired
	private ITeamService teamService;
/*
	@ResponseBody
	public boolean addTeam(@RequestBody Team team) {
		return teamService.saveTeam(team);
	}
	
	@ResponseBody
	public boolean addAllTeams (@RequestBody List<Team> Teams) {
		return teamService.saveAllTeam(Teams);
	}

	@ResponseBody
	public boolean deleteTeamByName(@RequestBody String name) {
		return teamService.deleteTeamByName(name);
	}
	
	@ResponseBody
	public boolean deleteOneTeam (@RequestBody int id) {
		return teamService.deleteOneTeam(id);
	}
	
	@ResponseBody
	public boolean updateTeam(@RequestBody Team team) {
		return teamService.updateTeam(team);
	}
	
	@ResponseBody
	public Team findOneTeam(@RequestBody int id) {
		return teamService.findTeamById(id);
	}
	
	@ResponseBody
	public Team findTeamByName(@RequestBody String name) {
		return teamService.findTeamByName(name);
	}

	@ResponseBody
	public List<Team> findAllTeams() {
		return teamService.findAllTeams();
	}
	*/
}
