package br.com.lab.app.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.lab.app.model.Coach;

public interface ICoachDAO extends CrudRepository <Coach, Long> {

}
