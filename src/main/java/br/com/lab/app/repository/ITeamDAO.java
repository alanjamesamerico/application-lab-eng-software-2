package br.com.lab.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.lab.app.model.Team;

public interface ITeamDAO extends CrudRepository <Team, Long> {
    
    @Query("select t from Team t where t.nome like %?1%")
    public List<Team> findByName(String nome);
    
    @Query("select c from Coach c where c.nome like %?1%")
    public List<Team> findByCoach(String nome);
}
