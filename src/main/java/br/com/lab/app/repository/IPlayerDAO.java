package br.com.lab.app.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.lab.app.model.Player;

public interface IPlayerDAO extends CrudRepository <Player, Long>{

}
