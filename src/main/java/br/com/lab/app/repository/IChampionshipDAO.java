package br.com.lab.app.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.lab.app.model.Championship;

public interface IChampionshipDAO extends CrudRepository<Championship, Long> {

}
