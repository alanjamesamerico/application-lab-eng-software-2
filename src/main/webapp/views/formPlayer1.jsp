<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@include file="../../views/head.jsp" %>

<h5>Cadastro de Jogadores</h5>

<form id="idForm" method="POST" class="col s12">
     <div class="row">
       <div class="input-field col s12">
         <input name="name" type="text" class="validate" />
         <label data-error="wrong" data-success="right">Nome do Jogador</label>
       </div>
       <div class="input-field col s12">
       	<input name="age" id="age" type="number" class="validate" />
        <label for="number" data-error="wrong" data-success="right">Idade</label>
       </div>
       <div class="input-field col s12">
         <input name="weight" id="weight" type="number" class="validate">
         <label for="number" data-error="wrong" data-success="right">Peso (Kg)</label>
       </div>
       <div class="input-field col s12">
         <input name="height" id="height" type="number" class="validate">
         <label for="number" data-error="wrong" data-success="right">Altura (m)</label>
      </div>
       <button class="btn waves-effect waves-light" type="submit" onclick="submitForm()">Cadastrar
         <i class="material-icons right">send</i>
       </button>
     </div>
</form>
<script>
	function submitForm() {
		(function ($) {
		    $.fn.serializeFormJSON = function () {
		        var o = {};
		        var a = this.serializeArray();
		        $.each(a, function () {
		            if (o[this.name]) {
		                if (!o[this.name].push) {
		                    o[this.name] = [o[this.name]];
		                }
		                o[this.name].push(this.value || '');
		            } else {
		                o[this.name] = this.value || '';
		            }
		        });
		        return o;
		    };
		})(jQuery);
		
		$("#idForm").submit(function(e) {
	    	var json = $(this).serializeFormJSON();
			alert("entrou: " + json);			
			$.ajax({
				headers: { 
			        'Accept': 'application/json',
			        'Content-Type': 'application/json' 
			    },
				url: '/app-lab/player/add',
	            type : "POST",
	            dataType : 'json',
	            data : JSON.stringify(json), 
	            success : function(data) {
	            	console.log(data);
	            },
	            error: function(xhr, resp, text) {
	            	if (data.status != 'OK')
	            	alert('Person has been added');
	            	else
	            	alert('Failed adding person: '
	            	+ data.status + ', '
	            	+ data.errorMessage);
	                console.log(xhr, resp, text);
	            }
	        });
		e.preventDefault();
		});
	}
</script>

