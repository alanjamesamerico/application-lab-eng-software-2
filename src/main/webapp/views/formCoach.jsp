<%@include file="../../views/head.jsp" %>

<h5>Cadastro do Treinador</h5>
<div class="row">
	<form action="" method="POST" id="idForm" class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <input id="nameCoach" type="text" class="validate" name="name">
          <label for="text" data-error="wrong" data-success="right">Nome do treinador</label>
        </div>
        <div class="input-field col s12">
          <input id="ageCoach" type="number" class="validate" name="age">
          <label for="number" data-error="wrong" data-success="right">Idade</label>
	     </div>
       <button class="btn waves-effect waves-light" type="submit" id="submit" onclick="submitForm()">Cadastrar
        <i class="material-icons right">send</i>
       </button>
	</form>
	<script>
		function submitForm() {
			(function ($) {
			    $.fn.serializeFormJSON = function () {
		
			        var o = {};
			        var a = this.serializeArray();
			        $.each(a, function () {
			            if (o[this.name]) {
			                if (!o[this.name].push) {
			                    o[this.name] = [o[this.name]];
			                }
			                o[this.name].push(this.value || '');
			            } else {
			                o[this.name] = this.value || '';
			            }
			        });
			        return o;
			    };
			})(jQuery);
			
			$("#idForm").submit(function(e) {
		    	var json = $(this).serializeFormJSON();
				alert("entrou: " + json);			
				$.ajax({
					headers: { 
				        'Accept': 'application/json',
				        'Content-Type': 'application/json' 
				    },
					url: '/app-lab/coach/add',
		            type : "POST",
		            dataType : 'json',
		            data : JSON.stringify(json), 
		            success : function(data) {
		            	console.log(data);
		            },
		            error: function(xhr, resp, text) {
		            	if (data.status != 'OK')
		            	alert('Person has been added');
		            	else
		            	alert('Failed adding person: '
		            	+ data.status + ', '
		            	+ data.errorMessage);
		                console.log(xhr, resp, text);
		            }
		        });
			e.preventDefault();
			});
		}
	</script>
</div>
