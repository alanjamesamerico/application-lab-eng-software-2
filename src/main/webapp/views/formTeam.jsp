<%@include file="../../views/head.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<h5>Cadastro de Time</h5>
<div class="row">
	<form id="idForm" method="POST" class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <input name="name" type="text" class="validate">
          <label for="text" data-error="wrong" data-success="right">Nome Time</label>
        </div>
        <div class="input-field col s12">
          <input name="flagImg" type="text" class="validate">
          <label for="text" data-error="wrong" data-success="right">Bandeira</label>
        </div>
        <button class="btn waves-effect waves-light" type="submit" onclick="submitForm()">Cadastrar
          <i class="material-icons right">send</i>
        </button>
      </div>
	</form>
</div>
<script>
	function submitForm() {
		(function ($) {
		    $.fn.serializeFormJSON = function () {
		        var o = {};
		        var a = this.serializeArray();
		        $.each(a, function () {
		            if (o[this.name]) {
		                if (!o[this.name].push) {
		                    o[this.name] = [o[this.name]];
		                }
		                o[this.name].push(this.value || '');
		            } else {
		                o[this.name] = this.value || '';
		            }
		        });
		        return o;
		    };
		})(jQuery);
		
		$("#idForm").submit(function(e) {
	    	var json = $(this).serializeFormJSON();
			alert("entrou: " + json);			
			$.ajax({
				headers: { 
			        'Accept': 'application/json',
			        'Content-Type': 'application/json' 
			    },
				url: '/app-lab/team/add',
	            type : "POST",
	            dataType : 'json',
	            data : JSON.stringify(json), 
	            success : function(data) {
	            	console.log(data);
	            },
	            error: function(xhr, resp, text) {
	            	if (data.status != 'OK')
	            	alert('Person has been added');
	            	else
	            	alert('Failed adding person: '
	            	+ data.status + ', '
	            	+ data.errorMessage);
	                console.log(xhr, resp, text);
	            }
	        });
		e.preventDefault();
		});
	}
</script>
