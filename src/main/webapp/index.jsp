<html lang="pt">
<!-- <%@include file="views/head.jsp" %> -->
<body> 
<div id="top" class="navbar-fixed">
  <nav class="grey darken-4" role="navigation">
    <div class="nav-wrapper grey darken-4">
      <div class="container">
        <a id="logo/-container" href="http://localhost:8080/app-lab/" class="brand-logo white-text">
         J - Game
        </a>
      </div>
    </div>
  </nav>
</div>
 <div class="row">
 <!-- COLUNA -->
  <div class="col s3">
  <div class="container">
    <font color="black">
    	<a href = "javascript:showLoad('../app-lab/views/formPlayer1.jsp')">
       	<p>Jogador</p>
     	</a>
	     <a href = "javascript:showLoad('../app-lab/views/formCoach.jsp')">
	       <p>Treinador</p>
	     </a>
	     <a href = "javascript:showLoad('../app-lab/views/formTeam.jsp')">
	       <p>Time</p>
	     </a>
	     <a href = "javascript:showLoad('../app-lab/views/formChampionship.jsp')">
	       <p>Campeonato</p>
	     </a>
     </font>
  </div>
  </div>
  <!-- COLUNA -->
  <div class="col s9">
   <div class="container">
     <div id="content"></div>
   </div>
  </div>
 </div>
 <script type="text/javascript">
 	window.onload = showLoad('../app-lab/views/init.jsp')
 </script>
 </body>
</html>