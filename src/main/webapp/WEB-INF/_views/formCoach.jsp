<%@include file="../../views/head.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="row">
	<form class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <input id="name_coach" type="text" class="validate">
          <label for="text" data-error="wrong" data-success="right">Nome do treinador</label>
        </div>
        <div class="input-field col s12">
          <input id="age_coach" type="number" class="validate">
          <label for="number" data-error="wrong" data-success="right">Idade</label>
	     </div>
	     <button class="btn waves-effect waves-light" type="submit" name="action">Cadastrar
	     	<i class="material-icons right">send</i>
	      </button>
       </div>
	</form>
</div>
