<%@include file="../../views/head.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="row">
	<form action="" class="col s12">
		 <div class="row">
		   <div class="input-field col s12">
		     <input id="name_champ" type="text" class="validate">
		     <label for="text" data-error="wrong" data-success="right">Nome do campeonato</label>
		   </div>
		   <div class="input-field col s12">
		    <input id="n_rounds" type="number" class="validate">
			<label for="text" data-error="wrong" data-success="right">N�mero de rodadas</label>
		</div>
		   <div class="input-field col s12">
		   	Data de In�cio
		     	<input id="i_date" type="date" class="validate">
		   </div>
		   <div class="input-field col s12">
		   	Data de Final
		     	<input id="f_date" type="date" class="validate">
		   </div>
		   <button class="btn waves-effect waves-light" type="submit" name="action">Cadastrar
		     <i class="material-icons right">send</i>
		   </button>
		 </div>
	</form>
</div>