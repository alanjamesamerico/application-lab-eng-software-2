<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>J</title>
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="../../lib/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="../../lib/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <!-- JS -->
  <script src="https://code.jquery.com/jquery-latest.min.js"></script>
</head>
<body>
	<form:form action="/player/add/" method="post" modelAttribute="player" model class="col s12">
	     <div class="row">
	       <div class="input-field col s12">
	         <form:input path="namePlayer" type="text" class="validate"/>
	         <form:label path="namePlayer" for="text" data-error="wrong" data-success="right">Nome do Jogador</form:label>
	       </div><!--
	       <div class="input-field col s12">
	          <input id="age" type="number" class="validate">
	          <label for="number" data-error="wrong" data-success="right">Idade</label>
	        </div>
	       <div class="input-field col s12">
	         <input id="weight" type="number" class="validate">
	         <label for="number" data-error="wrong" data-success="right">Peso (Kg)</label>
	       </div>
	       <div class="input-field col s12">
	         <input id="height" type="number" class="validate">
	         <label for="number" data-error="wrong" data-success="right">Altura (m)</label>
	      </div>-->
	       <button class="btn waves-effect waves-light" type="submit">Cadastrar
	         <i class="material-icons right">send</i>
	       </button>
	     </div>
	</form:form>
</body>
</html>